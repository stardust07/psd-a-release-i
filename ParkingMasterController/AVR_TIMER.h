/*
 * AVR_TIMER.h
 *
 *  Created on: Nov 27, 2017
 *      Author: evidence
 */

#ifndef AVR_TIMER_H_
#define AVR_TIMER_H_
#include "AVR_TIMER.c"


void Timer0_init();
void Timer0_Start();
void Timer0_Stop();
void Timer0_Clear();
unsigned char Timer0_getValue();
/*---------------------------------------------------*/
void Timer1_start();
void Timer1_stop();
void Timer1_clear();
unsigned char Timer1_getValue();
/*---------------------------------------------------*/
void Timer2_init();
void Timer2_Start();
void Timer2_Stop();
void Timer2_Clear();
unsigned char Timer2_getValue();



#endif /* AVR_TIMER_H_ */
