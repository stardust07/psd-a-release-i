/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2014  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation,
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

#include "avr/io.h"
#include "Avr_DIO.h"
#include "AVR_TIMER.h"
#include "util/delay.h"
#include "avr/interrupt.h"
#include "Ultrasonic.h"
#include "Arduino.h"
#include "ee.h"
#include "stdint.h"
#include "spi.h"
#include "UART_lib.h"

unsigned char global_enable;

void loop(void)
{
	//UART functions here
	//If slot + assist global_enable = 'S'
	//if only the parking assist global_enable = 'P'
	//for now assume that global_enable = 'S'
	//global_enable = 'S';
	global_enable = UART_RxChar();
	ActivateTask(Comm);
	ActivateTask(BT);

}

void setup(void)
{

	SPI_initMaster();

	DDRD |= (1 << PD2);
	PORTD |=(1 << PD2);

	//Setting SLave selects as outputs
	//PB2, Pin 10 activates parking assist board
	//PB1, Pin 9 activate slot detection board
	DDRB |= (1 <<PB2);
	DDRB |= (1 <<PB1);

	PORTB |= (1 <<PB2);
	PORTB |= (1 <<PB1);

	UART_init(4800,8,1,0);
}

int main(void)
{
	EE_mcu_init();

	init();

#if defined(USBCON)
	USBDevice.attach();
#endif
	
	setup();

	for (;;) {
		loop();
		if (serialEventRun) serialEventRun();
	}

	return 0;

}
