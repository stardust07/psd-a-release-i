/*
 * AVR_TIMER.c
 *
 *  Created on: Nov 27, 2017
 *      Author: evidence
 */

#include "AVR_TIMER.h"
#include "avr/io.h"

void Timer0_init(){
	TCCR0A =0;
	TCCR0B = 0xC0; 			//No Prescaling FOC0 and F0C01 are set ( NO PWM )
	TIMSK0 = 0x01;			//TOIE0 set, to enable timer overflow interrupt
	SREG |= (1<<7); 		// Activate General Interrupt

}
void Timer0_Start(){
	TCCR0B |= 0x02; 		//8 prescalar

}
void Timer0_Stop(){

	TCCR0B |= 0xC0; 		//Set FOC to reset the counter  and CS0->2 are set 000 to stop clock source off the register
}
void Timer0_Clear(){
	TCNT0 =0; 				//Clear the count register
}
unsigned char Timer0_getValue(){
	return TCNT0; 			// return the value in count register
}
//======Timer 1======
/* This function configures the timer to operate without any prescaler and resets
 * the timer counter, it can be used to enable overflow interrupts by removing
 * the comments in the function. */
// arguments: none
// return: void
void Timer1_start()
{
	// setup 16 bit timer & enable interrupts, timer increments to 65535 //and interrupts on overflow
	TCCR1A = 0X00;
	TCCR1B = (0<<CS12)|(1<<CS11)|(0<CS10); // select internal clock with 1/8 prescaling
	TCNT1 = 0; // reset counter to zero

	// This part is used when using overflow interrupts
	/* TIMSK1 = 1<<TOIE1; // enable timer interrupt
	sei(); // enable all(global) interrupts */
}

/* This function stops the entire timer by clearing its clock, and it also
 * resets the counter timer. */
// arguments: none
// return: void
void Timer1_stop()
{
	TCCR1B = (0<<CS02)|(0<<CS01)|(0<<CS00); // stop timer
	TCNT1 = 0; // reset counter to zero
	//timerCounter = 0; // reset the overflow counter
}

/* This function only resets the counter so that it starts again from zero,
 * but it doesn't stop the counter. */
// arguments: none
// return: void
void Timer1_clear()
{
	TCNT1 = 0;
}

/* This function returns the value of the counter at the time it is called. */
// arguments: none
// return: uint16_t
unsigned char Timer1_getValue()
{
	return TCNT1;
}

// timer overflow interrupt, each time when timer value passes 65355 value
/*ISR(TIMER1_OVF_vect)
{
	cli();
	timerCounter++; // count the number of overflows
	// dont wait too long for the sonar end response, stop if time for measuring the distance exceeded limits
	uint32_t ticks = timerCounter * 65535 + TCNT1;
	if (ticks > 174927)
	{
		// timeout
		DDRB |= 0x10;
		PORTB |= 0x10;
		running = 0; // ultrasound scan done
		distance = MAX_DISTANCE + 2; // show that measurement failed with a timeout (could return max distance here if needed)
		timer_stop();
	}
}
*/
//===================

void Timer2_init(){
	TCCR2A =0;
	TCCR2B = 0xC0; 			//No Prescaling FOC0 and F0C01 are set ( NO PWM )
	TIMSK2 = 0x01;			//TOIE0 set, to enable timer overflow interrupt
	SREG |= (1<<7); 		// Activate General Interrupt

}
void Timer2_Start(){
	TCCR2B |= 0x02; 		//8 prescalar

}
void Timer2_Stop(){

	TCCR2B |= 0xC0; 		//Set FOC to reset the counter  and CS0->2 are set 000 to stop clock source off the register
}
void Timer2_Clear(){
	TCNT2 =0; 				//Clear the count register
}
unsigned char Timer2_getValue(){
	return TCNT2; 			// return the value in count register
}
