/*
 * SPI.h
 *
 *
 */ 

#ifndef SPI_H_
#define SPI_H_

#include "stdint.h"
#include <avr/io.h>
#include "SPI.c"

void SPI_clear(void);
void SPI_initMaster(void); 
void SPI_initSlave(void);
void SPI_sendByte(const uint8_t data);
uint8_t SPI_recieveByte(void);
void SPI_sendString(const char *Str);
void SPI_receiveString(char *Str);
void SPI_sendChar(const unsigned char data);
unsigned char SPI_recieveChar(void);
#endif
