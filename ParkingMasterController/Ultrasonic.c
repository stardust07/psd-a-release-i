/*
 * Ultrasonic.c
 *
 *  Created on: Sep 9, 2017
 *      Author: Assassin
 *
 *  This driver is meant only Ultrasonic Sensor HC-SR04. You can
 *  initialize the sensor using any GPIO pins and the known detection range.
 */

#include "ee.h"
#include "avr/io.h"
#include <avr/interrupt.h>
#include "AVR_TIMER.h"
#include "Avr_DIO.h"
#include "stdint.h"
#include "util/delay.h"


volatile uint16_t distance;

void U_INIT(uint8_t triggerPIN, uint8_t triggerPORT, uint8_t echoPIN, uint8_t echoPORT)
{

	pin_Dir(triggerPORT, triggerPIN, 1);
	pin_Dir(echoPORT, echoPIN, 0);


}


void U_TRIGGER(uint8_t triggerPIN,uint8_t triggerPORT)
{
	pin_Write(triggerPORT, triggerPIN, 0);	//Clear trigger pin
	_delay_us(2);					//Delaying for 2u seconds to make sure the Port  is ready.
	pin_Write(triggerPORT, triggerPIN, 1);	//Setting the pIn.
	_delay_us(10);					//Delaying for 10u seconds for the second wave to be sent.
	pin_Write(triggerPORT, triggerPIN, 0);	//Then again Clear the Pin.
}

uint16_t U_GET_PULSEWIDTH(uint8_t echoPIN,uint8_t echoPORT)
{
	uint32_t i,result;

	//Wait for the rising edge
	for(i = 0; i < 6000000; i++)
	{
		if(!pin_State(echoPORT, echoPIN)) continue;
		else	break;
	}

	if(i == 6000000)
		return 0xffff; //Indicates time out

	//High Edge Found

	//Setup Timer1 to start counting

			       TCCR1A=0X00;
			       TCCR1B=(1<<CS11); //Prescaler = Fcpu/8
			       TCNT1=0x00;       //Init counter

	//Now wait for the falling edge
	for(i = 0; i < 6000000; i++)
	{
		if(pin_State(echoPORT, echoPIN))
		{
			if(Timer1_getValue() > 60000) break;
			else	continue;
		}
		else
			break;
	}

	if(i == 6000000)
		return 0xffff; //Indicates time out

	//Falling edge found
	result = TCNT1;

	//Stop Timer
	 TCCR1B=0x00;

	if(result > 60000)
		return 0xfffe; //No obstacle
	else
		return (result>>1);
}

uint16_t U_GET_DISTANCE(uint16_t duration)
{
	 distance = ((duration * 0.0343) / 2);
	return distance;
}
