/*
 * I2C.h
 *
 *
 */


#ifndef I2C_H_
#define I2C_H_

#define F_CPU 8000000UL    /* Define CPU clock Frequency 8MHz */
///#include <string.h>   /* Include string header file */


#define Slave_Address   0x20
#define Slave_Write_Address   0x20
#define Slave_Read_Address    0x21
#define  count 10
/* Define bit rate */
#define BITRATE(TWSR)  ((F_CPU/SCL)-16)/(2*pow(4,(TWSR&((1<<TWPS0)|(1<<TWPS1)))))


void I2C_Init();                             /* I2C initialize function */
uint8_t I2C_Start(char write_address);/* I2C start function */
uint8_t I2C_Repeated_Start(char read_address);/* I2C repeated start function */
uint8_t I2C_Write(char data);/* I2C write function */
char I2C_Read_Ack();/* I2C read ack function */
char I2C_Read_Nack();/* I2C read nack function */
void I2C_Stop();     /* I2C stop function */
void I2C_Slave_Init(uint8_t slave_address);
int8_t I2C_Slave_Listen();

int8_t I2C_Slave_Transmit(char data);
char I2C_Slave_Receive();







#include "stdint.h"
#include "i2c.c"
#endif /* I2C_H_ */
