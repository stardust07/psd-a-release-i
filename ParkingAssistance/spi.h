/*
 * SPI.h
 *
 *
 */ 

#ifndef SPI_H_
#define SPI_H_

#include "stdint.h"
#include <avr/io.h>
#include "SPI.c"


void SPI_initMaster(void); 
void SPI_initSlave(void);
void SPI_sendByte(const uint8_t data);
uint8_t SPI_recieveByte(void);
void SPI_sendString(const unsigned char *Str);
void SPI_receiveString(char *Str);
void SPI_sendChar(const char data);
char SPI_recieveChar(void);

#endif
