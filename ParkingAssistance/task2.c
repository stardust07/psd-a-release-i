/*
 * task2.c
 *
 *  Created on: Sep 7, 2017
 *      Author: evidence
 */

#include "ee.h"
#include "UART_lib.h"
#include "stdint.h"
#include "spi.h"
#include "util/delay.h"

extern volatile unsigned char Send_Data[6];
extern volatile unsigned char sensor1;
extern volatile unsigned char sensor2;
extern volatile unsigned char sensor3;
extern uint8_t disToObject[3];
extern volatile char command;

unsigned char test_data[2];

TASK(Send){
	test_data[0] = 'X';
	test_data[1] = '\0';
	/*test_data[2] = 'Y';
	test_data[3] = ',';
	test_data[4] = 'Z';
	test_data[5] = '\0';*/

	if (command == 'P')
	{
		_delay_ms(1);
		PORTD = PORTD | (1 << PD2);
		//SPI_SendString(test_data);
		SPI_sendChar('<');
		SPI_sendChar(sensor1);
		SPI_sendChar(sensor2);
		SPI_sendChar(sensor3);
		/*SPI_sendChar('X');
		SPI_sendChar('Y');
		SPI_sendChar('Z');*/
		SPI_sendChar('>');

		_delay_ms(1);
		PORTD = PORTD &~ (1 << PD2);
	}
};
