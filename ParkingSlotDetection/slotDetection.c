/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2014  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation,
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

#include "avr/io.h"
#include "Avr_DIO.h"
#include "util/delay.h"
#include "Ultrasonic.h"
#include "avr/interrupt.h"
#include "stdint.h"

#define CARWIDTH  7

volatile int overflows = 0;
//Sensors' distances.
unsigned int sensor1_dist 	= 0;
unsigned int sensor2_dist 	= 0;

//Hitting Flags
volatile uint8_t flag=0;
volatile uint8_t flag_sensor1=0;
volatile uint8_t flag_sensor2=0;

 uint8_t slot_flag;

unsigned int duration;
extern volatile unsigned char send_or_receive;

unsigned int Hit(unsigned int dist);

ISR(TIMER2_OVF_vect){

	if(flag ==0){
		overflows++;
	}
	else if (flag == 1 && overflows>0){
		overflows--;
	}

}

TASK(slotDetection) {

	if(slot_flag != 1 && send_or_receive =='S'){

		U_TRIGGER(PB0, B);
		duration = U_GET_PULSEWIDTH(PB1, B);
		sensor1_dist = U_GET_DISTANCE(duration);

		U_TRIGGER(PD6, D);
		duration = U_GET_PULSEWIDTH(PD7, D);
		sensor2_dist = U_GET_DISTANCE(duration);

		if  (Hit(sensor1_dist)==1)
		{
			Timer2_Start();
			flag_sensor1 = 1;
			PORTD |= (1<<PD4);
		}
		else {
			if(flag_sensor2==0){
				overflows=0;
			}
			else{
				if(overflows==0 && flag==1){
					PORTD |= (1<<PD2);
					slot_flag = 1;
					//_delay_ms(500);
					Timer2_Clear();
					Timer2_Stop();
				}
				else{
					//No parking slot flag
					PORTD &=~ (1<<PD2);
				   //_delay_ms(500);
				   Timer2_Clear();
				   Timer2_Stop();
				}
			}
			PORTD &=~ (1<<PD4);
			flag_sensor1=0;
			Timer2_Clear();
			Timer2_Stop();
		}
		if(Hit(sensor2_dist)==1) {
			PORTD |= (1<<PD3);
		}
		else{
			PORTD &=~ (1<<PD3);
		}
		if(Hit(sensor2_dist)==1 && flag_sensor1==1)
		{
			if(flag != 1){
				overflows +=1;
				overflows /= 2;
			}

		   Timer2_Clear();
		   Timer2_Stop ();
		   flag =1;
		   flag_sensor2=1;

		}
		else{
			flag_sensor2=0;

		}
	   if(overflows==0 && flag==1){
	   	   // parking slot detected
		   PORTD |= (1<<PD2);
		   slot_flag = 1;
		   //_delay_ms(500);
		   Timer2_Clear();
		   Timer2_Stop();

	   }

	}

};


unsigned int Hit(unsigned int dist){
	if( (dist) >= 10){
		return 1;
	}
	else {
		return 0 ;
	}

}

void reset_system(){
	PORTD &= ~ (1<<PB4);
	PORTD &= ~ (1<<PB3);
	PORTD &= ~ (1<<PD2);
	flag=0;
	flag_sensor1=0;
	flag_sensor2=0;
	sensor1_dist=0;
	sensor2_dist=0;

}


