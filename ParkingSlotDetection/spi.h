/*
 * SPI.h
 *
 *
 */ 

#ifndef SPI_H_
#define SPI_H_

#include "stdint.h"
#include <avr/io.h>
#include "SPI.c"


void SPI_initMaster(void); 
void SPI_initSlave(void);
void SPI_sendByte(const uint8_t data);
uint8_t SPI_recieveByte(void);
void SPI_sendString(const uint8_t *Str);
void SPI_receiveString(char *Str);
char SPI_recieveChar(void);
void SPI_sendChar(const char data);

#endif
