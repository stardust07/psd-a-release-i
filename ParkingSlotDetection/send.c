#include "ee.h"
#include "Arduino.h"
#include "stdint.h"
#include "spi.h"
#include "util/delay.h"

extern reset_system();
extern uint8_t  slot_flag;

extern volatile unsigned char send_or_receive;

unsigned int flag_send = 0;

TASK(send){


	if(send_or_receive =='S' && slot_flag ==1){ //now i'm active

		flag_send = 1;
		//Delay important
		_delay_us(100);
		slot_flag=-1;
		SPI_sendChar('A');
		_delay_ms(1000);
		//send_or_receive='Z'; ///Deactivate urself
		reset_system();
		PORTD |= (1 << PD4);
		_delay_ms(2000);
		PORTD &= ~(1 << PD4);
	}

};
