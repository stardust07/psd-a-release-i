/*
 * SPI.c
 *
 *  Created on: Nov 14, 2017
 *      Author: evidence
 */

#include "spi.h"
#include "ee.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "stdint.h"

void SPI_initMaster(void)
{
	//******** Configure SPI Master Pins *********
	//SS(PB2)   --> Output
	//MOSI(PB3) --> Output
    //MISO(PB4) --> Input
	// SCK(PB5) --> Output

	DDRB = DDRB | (1<<PB2);
	DDRB = DDRB | (1<<PB3);
	DDRB = DDRB & ~(1<<PB4);
	DDRB = DDRB | (1<<PB5);

/*
	pin_Dir(B, 2, output);
	pin_Dir(B, 3,output );
	pin_Dir(B, 4, input);
	pin_Dir(B, 5,output);
*/

	// ************ Enable SPI + enable Master + choose SPI clock = Fosc/4
	    SPCR = (1<<SPE) | (1<<MSTR) | (1 << SPI2X) | (1 << SPR0);
	    // SET_BIT(SPCR, SPE);
	    // SET_BIT(SPCR, MSTR);
}

void SPI_initSlave(void)
{
	//******** Configure SPI Slave Pins *********
	//SS(PB2)   --> Input
	//MOSI(PB3) --> Input
	//MISO(PB4) --> Output
	// SCK(PB5) --> Input

	DDRB = DDRB & (~(1<<PB2));
	DDRB = DDRB & (~(1<<PB3));
	DDRB = DDRB | (1<<PB4);
	DDRB = DDRB & (~(1<<PB5));

	/*
	    pin_Dir(B, 2, input);
		pin_Dir(B, 3,input );
		pin_Dir(B, 4, output);
		pin_Dir(B, 5,input);
*/

		// just enable SPI + choose SPI clock = Fosc/4
	    SPCR = (1<<SPE);
		//SET_BIT(SPCR, SPE);
}

void SPI_sendByte(const uint8_t data)
{
	SPDR = data; //send data by SPI
	while(!(SPSR & (1<<SPIF))); //wait until SPI interrupt flag=1 (data is sent correctly)
}

uint8_t SPI_recieveByte(void)
{
	while(!(SPSR & (1<<SPIF))); //wait until SPI interrupt flag=1(data is receive correctly)
   return SPDR; //return the received byte from SPI data register
}

void SPI_sendChar(const char data)
{
	SPDR = data; //send data by SPI
	while(!(SPSR & (1<<SPIF))); //wait until SPI interrupt flag=1 (data is sent correctly)
}

char SPI_recieveChar(void)
{
	while(!(SPSR & (1<<SPIF))); //wait until SPI interrupt flag=1(data is receive correctly)
   return SPDR; //return the received byte from SPI data register
}

void SPI_SendString(const uint8_t *Str)
{
	uint8_t i = 0;
	while(Str[i] != '\0')
	{
		SPI_sendByte(Str[i]);
		i++;
	}
}

void SPI_ReceiveString(char *Str)
{
	unsigned char i = 0;
	Str[i] = SPI_recieveByte();
	while(Str[i] != '#')
	{
		i++;
		Str[i] = SPI_recieveByte();
	}
	Str[i] = '\0';
}
