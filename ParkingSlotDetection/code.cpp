/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2014  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation,
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

#include "avr/io.h"
#include "Avr_DIO.h"
#include "AVR_TIMER.h"
#include "util/delay.h"
#include "avr/interrupt.h"
#include "Ultrasonic.h"
#include "Arduino.h"
#include "ee.h"
#include "stdint.h"
#include "spi.h"
extern uint8_t slot_flag; //if set one send it to the other board
volatile unsigned char send_or_receive;


void loop(void)
{
	send_or_receive = SPI_recieveChar();
	ActivateTask(slotDetection);
	ActivateTask(send);
}

void setup(void)
{


	Timer2_init();
	U_INIT(PB0, B, PB1, B);
	U_INIT(PD6, D, PD7, D);

	SPI_initSlave();

	//Debugging LEDs
	pin_Dir(D,PD2,output);
	pin_Dir(D,PD3,output);
	pin_Dir(D,PD4,output);

	PORTB &= ~ (1<<PB5);

}

int main(void)
{
	EE_mcu_init();

	init();

#if defined(USBCON)
	USBDevice.attach();
#endif
	
	setup();

	for (;;) {
		loop();
		if (serialEventRun) serialEventRun();
	}

	return 0;

}
